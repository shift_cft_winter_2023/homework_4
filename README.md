# Yolo v5

* ноутбук бы выполнен в google colab
* данные: zip - архив, который лежит по следующему пути /content/archive.zip 
* обучение 255 изображений, валидация 100 изображений
* модель обучалась на 4х батчах, 200 эпох, с замороженным бэкбоном, остальные параметры использованы по умолчанию
* логгер wandb.ai, [график лосса](https://wandb.ai/cv_shift/dataset/reports/val-box_loss-23-03-18-14-48-37---VmlldzozODIwMzUx?accessToken=p818kmqzsftyoewv1vp110b9ukp25r489c7ada54z7ktoc27cqto0ykwuwp358uz)
